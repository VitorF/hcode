<?php  
//tipos de variaveis primitivos 

$nome = "vitor";
$site = 'www.site.com';
$salario = 5500.99;
$ano = 1995;

//Variaveis complexos variasveis tipo objeto e array

$nascimento  =  new DateTime(); // varivale tipo Objeto

echo "</br>";
echo "</br>";
echo "Tipo Objeto ";
echo "</br>";
echo var_dump($nascimento);

$frutas  =  array("acerola","laranja","maracuja");

echo "</br>";
echo "Tipo Array :";
echo "</br>";
echo var_dump($frutas);

// Variaveis especiais tipo null e resource 
$arquivo  =  fopen("exemplo-03.php","r");
echo "</br>";
echo "</br>";
echo "Tipo Resources";
echo "</br>";
echo var_dump($arquivo);


$vazia  =   null;

echo "</br>";echo "</br>";
echo "Tipo null";
echo "</br>";
echo var_dump($vazia);



?>